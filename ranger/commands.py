from ranger.api.commands import Command
import os
from ranger.core.loader import CommandLoader


class sw(Command):
    def execute(self):
        """ Unmount all devices in directroy """
        def refresh(_):
            cwd = self.fm.get_directory(original_path)
            cwd.load_content()

        cwd = self.fm.thisdir
        original_path = cwd.path

        descr = 'running i3-swallow...'
        obj = CommandLoader(
            args=['i3-swallow', 'xdg-open', self.fm.thisfile.relative_path],
            descr=descr)

        obj.signal_bind('after', refresh)
        self.fm.loader.add(obj)


class unmount(Command):
    def execute(self):
        """ Unmount all devices in directroy """
        def refresh(_):
            cwd = self.fm.get_directory(original_path)
            cwd.load_content()

        cwd = self.fm.thisdir
        original_path = cwd.path
        au_flags = []
        for folder in os.listdir(cwd.path):
            au_flags += [os.path.join(cwd.path, folder)]

        descr = 'unmounting drives...'
        obj = CommandLoader(args=['umount'] + au_flags, descr=descr)

        obj.signal_bind('after', refresh)
        self.fm.loader.add(obj)


class detach(Command):
    def execute(self):
        """ Unmount all devices in directroy """
        def refresh(_):
            cwd = self.fm.get_directory(original_path)
            cwd.load_content()

        cwd = self.fm.thisdir
        original_path = cwd.path
        au_flags = []
        for folder in os.listdir(cwd.path):
            au_flags += [os.path.join(cwd.path, folder)]

        descr = 'detaching drives...'
        obj = CommandLoader(args=['detach'] + au_flags, descr=descr)

        obj.signal_bind('after', refresh)
        self.fm.loader.add(obj)


class extracthere(Command):
    def execute(self):
        """ Extract copied files to current directory """
        copied_files = tuple(self.fm.copy_buffer)

        if not copied_files:
            return

        def refresh(_):
            cwd = self.fm.get_directory(original_path)
            cwd.load_content()

        one_file = copied_files[0]
        cwd = self.fm.thisdir
        original_path = cwd.path
        au_flags = ['-X', cwd.path]
        au_flags += self.line.split()[1:]
        au_flags += ['-e']

        self.fm.copy_buffer.clear()
        self.fm.cut_buffer = False
        if len(copied_files) == 1:
            descr = "extracting: " + os.path.basename(one_file.path)
        else:
            descr = "extracting files from: " +\
                os.path.basename(one_file.dirname)
        obj = CommandLoader(args=['aunpack'] + au_flags +
                            [f.path for f in copied_files],
                            descr=descr)

        obj.signal_bind('after', refresh)
        self.fm.loader.add(obj)


# https://github.com/ranger/ranger/wiki/Integrating-File-Search-with-fzf
# Now, simply bind this function to a key, by adding this to your
# ~/.config/ranger/rc.conf: map <C-f> fzf_select
class fzf_select(Command):
    """
    :fzf_select

    Find a file using fzf.

    With a prefix argument select only directories.

    See: https://github.com/junegunn/fzf
    """
    def execute(self):
        import subprocess
        if self.quantifier:
            # match only directories
            command = "fd -t d | fzf +m"
            # command="find -L . \( -path '*/\.*' -o -fstype 'dev' -o -fstype 'proc' \) -prune \
            # -o -type d -print 2> /dev/null | sed 1d | cut -b3- | fzf +m"
            # command="ag -l --path-to-ignore ~/.ignore --nocolor --hidden -g \"\" | fzf +m"
        else:
            command = "fd | fzf +m"
            # match files and directories
            # command="find -L . \( -path '*/\.*' -o -fstype 'dev' -o -fstype 'proc' \) -prune \
            # -o -print 2> /dev/null | sed 1d | cut -b3- | fzf +m"
            # command="ag -l --path-to-ignore ~/.ignore --nocolor --hidden -g \"\" | fzf +m"
        fzf = self.fm.execute_command(command, stdout=subprocess.PIPE)
        stdout, stderr = fzf.communicate()
        if fzf.returncode == 0:
            fzf_file = os.path.abspath(stdout.decode('utf-8').rstrip('\n'))
            if os.path.isdir(fzf_file):
                self.fm.cd(fzf_file)
            else:
                self.fm.select_file(fzf_file)


# fzf_locate
class fzf_locate(Command):
    """
    :fzf_locate

    Find a file using fzf.

    With a prefix argument select only directories.

    See: https://github.com/junegunn/fzf
    """
    def execute(self):
        import subprocess
        if self.quantifier:
            # command="locate home media | fzf -e -i"
            command = "fd | fzf -e -i"
        else:
            # command="locate home media | fzf -e -i"
            command = "fd | fzf -e -i"
        fzf = self.fm.execute_command(command, stdout=subprocess.PIPE)
        stdout, stderr = fzf.communicate()
        if fzf.returncode == 0:
            fzf_file = os.path.abspath(stdout.decode('utf-8').rstrip('\n'))
            if os.path.isdir(fzf_file):
                self.fm.cd(fzf_file)
            else:
                self.fm.select_file(fzf_file)
