# Created by newuser for 5.0.7
# Extended by Chavdar Dutsov
typeset -A key

path+=('/home/chavdar/.scripts/')
path+=('/home/chavdar/.local/share/cargo/bin/')
path+=('/home/chavdar/.local/bin/')
path+=('/home/chavdar/.local/share/gem/ruby/3.0.0/bin')
export PATH
LD_LIBRARY_PATH=/usr/lib/root/lib/
export LD_LIBRARY_PATH
ROOTSYS=/usr/lib/root
export ROOTSYS
export VDPAU_DRIVER=va_gl
export LIBVA_DRIVER_NAME=iHD


setopt autocd
setopt completealiases
zstyle ':completion:*' menu select
setopt CORRECT

setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS
setopt INC_APPEND_HISTORY
setopt HIST_IGNORE_DUPS
setopt EXTENDED_HISTORY
# setopt SHARE_HISTORY
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt INC_APPEND_HISTORY_TIME
HISTFILE=~/.config/zsh/history
HISTSIZE=10000000
SAVEHIST=10000000


[[ -n "${key[PageUp]}"   ]]  && bindkey  "${key[PageUp]}"    history-beginning-search-backward
[[ -n "${key[PageDown]}" ]]  && bindkey  "${key[PageDown]}"  history-beginning-search-forward

autoload -U promptinit
promptinit

setopt autopushd pushdsilent pushdtohome

## Remove duplicate entries
setopt pushdignoredups

## This reverts the +/- operators.
setopt pushdminus


autoload -U run-help
autoload run-help-git
autoload run-help-svn
autoload run-help-svk
alias help=run-help


#-----------------------------
## Dircolors
##-----------------------------
LS_COLORS="no=00:fi=00:di=01;36:ln=01;36:pi=40;33:so=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;01;31:ex=01;32:*.cmd=01;32:*.exe=01;32:*.sh=01;32:*.gz=00;31:*.bz2=00;31:*.bz=00;31:*.tz=00;31:*.rpm=00;31:*.cpio=00;31:*.t=93:*.pm=00;36:*.pod=00;96:*.conf=00;33:*.off=00;9:*.jpg=00;94:*.png=00;94:*.xcf=00;94:*.JPG=00;94:*.gif=00;94:*.pdf=00;91:*.tex=00;95:*.p=01;93:*.xls=00;36";
EXA_COLORS="no=00:fi=00:di=1;36:ln=1;36:pi=40;33:so=1;35:bd=4;33;01:cd=4;33;01:or=4;31;01:mi=01;01;31:ex=01;32:*.cmd=1;32:*.exe=1;32:*.sh=1;32:*.gz=00;31:*.bz2=00;31:*.bz=00;31:*.tz=00;31:*.rpm=00;31:*.cpio=00;31:*.t=33:*.pm=00;36:*.pod=00;36:*.conf=00;33:*.off=00;3:*.jpg=00;34:*.png=00;34:*.xcf=00;34:*.JPG=00;34:*.gif=00;34:*.pdf=00;31:*.tex=00;35:*.p=1;33:*.xls=00;36";
export LS_COLORS
export EXA_COLORS

#------------------------------
## ShellFuncs
##------------------------------
## -- coloured manuals
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;38;5;74m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[38;33;246m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[04;38;5;146m'

#------------------------------
# Window title
#------------------------------
case $TERM in
    alacritty|termite|*xterm*|rxvt|rxvt-unicode|rxvt-256color|rxvt-unicode-256color|(dt|k|E)term)
    precmd () {
        vcs_info
        window_title="\033]0;${PWD##*/} $1\007"
        echo -ne "$window_title"
    } 
    preexec () {
        print -Pn "\e]0;$1\a"
    }
;;
esac

#------------------------------
# Prompt
#------------------------------

# Spaceship prompt settings
autoload -U promptinit; promptinit
prompt spaceship
SPACESHIP_BATTERY_SHOW=false
SPACESHIP_VI_MODE_SHOW=false
# SPACESHIP_CHAR_SYMBOL="❯ "
SPACESHIP_CHAR_SYMBOL="➜ "

# Remove mode switching delay.
KEYTIMEOUT=5

#------------------------------
# Vim bindings
#------------------------------
# Change cursor shape for different vi modes.
function zle-keymap-select {
    if [[ ${KEYMAP} == vicmd ]] ||
        [[ $1 = 'block' ]]; then
            echo -ne '\e[1 q'

        elif [[ ${KEYMAP} == main ]] ||
            [[ ${KEYMAP} == viins ]] ||
            [[ ${KEYMAP} = '' ]] ||
            [[ $1 = 'beam' ]]; then
                    echo -ne '\e[5 q'
    fi
}
zle -N zle-keymap-select
#Vim bindings
bindkey -v

#------------------------------
# Misc stuff
#------------------------------
# extended globbing, awesome!
setopt extendedGlob

# zmv -  a command for renaming files by means of shell patterns.
autoload -U zmv

# zargs, as an alternative to find -exec and xargs.
autoload -U zargs

# Turn on command substitution in the prompt (and parameter expansion and arithmetic expansion).
setopt promptsubst

# Control-x-e to open current line in $EDITOR, awesome when writting functions or editing multiline commands.
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^x^e' edit-command-line
bindkey -M viins    "" backward-delete-char

# Unlock the gnome keyring
if [ -n "$DESKTOP_SESSION" ];then
    eval $(gnome-keyring-daemon --start)
    export SSH_AUTH_SOCK
fi

# Completion.
autoload -Uz compinit
compinit
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*' list-colors ${(s.:.)EXA_COLORS}
zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' menu select=2
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
zstyle ':completion::complete:*' use-cache 1
zstyle ':completion:*:descriptions' format '%U%F{cyan}%d%f%u'


# Aliases
alias cp='cp -iv'
alias rcp='rsync -v --progress'
alias rmv='rsync -v --progress --remove-source-files'
alias mv='mv -iv'
alias rm='rm -iv'
alias rmdir='rmdir -v'
alias ln='ln -v'
alias chmod="chmod -c"
alias chown="chown -c"
alias ll="exa -lah --group-directories-first --color=auto"

alias grep='grep --colour=auto'
alias egrep='egrep --colour=auto'
alias ls='exa --color=auto  --group-directories-first'

alias ..='cd ..'
alias vzsh="vim ~/.config/zsh/.zshrc && source ~/.config/zsh/.zshrc"
alias vrc="vim ~/.config/vim/vimrc"
alias tube="mpsyt"
#Set stty alias
alias set-stty="stty cs8 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts -F"
alias sconv="smart-units"

alias wine32="WINEARCH=win32 WINEDEBUG=-all WINEPREFIX='/home/chavdar/.wine32'"
alias r="ranger"
alias vim="nvim"
alias v="nvim"
alias ...="cd ../.."
alias peaclock="peaclock --config ~/.config/peaclock/config"
alias home-git="GIT_SSH_COMMAND=\"ssh -i ~/.ssh/id_ed25519\"\
    git --work-tree=$HOME/.config --git-dir="$XDG_CONFIG_HOME"/home-git"
alias gpg2="gpg2 --homedir "$XDG_DATA_HOME"/gnupg"
alias freecad="freecad -u "$XDG_CONFIG_HOME"/FreeCAD/user.cfg -s "$XDG_CONFIG_HOME"/FreeCAD/system.cfg"
alias mus="mpd-start && ncmpcpp"
alias bt="bluetoothctl"
alias mpv="i3-swallow mpv"
alias zathura="i3-swallow zathura"
alias arandr="i3-swallow arandr"
alias sw="i3-swallow xdg-open"
alias newsboat="newsboat -C "$XDG_CONFIG_HOME"/newsboat/config"
alias ssh="TERM=xterm ssh"
alias feh="feh --auto-zoom --scale-down --auto-rotate"

insert_sudo () { zle beginning-of-line; zle -U "sudo " }
zle -N insert-sudo insert_sudo
bindkey "^[s" insert-sudo

# Keypad
# 0 . Enter
bindkey -s "^[Op" "0"
bindkey -s "^[Ol" "."
bindkey -s "^[OM" "^M"
# 1 2 3
bindkey -s "^[Oq" "1"
bindkey -s "^[Or" "2"
bindkey -s "^[Os" "3"
# 4 5 6
bindkey -s "^[Ot" "4"
bindkey -s "^[Ou" "5"
bindkey -s "^[Ov" "6"
# 7 8 9
bindkey -s "^[Ow" "7"
bindkey -s "^[Ox" "8"
bindkey -s "^[Oy" "9"
# + -  * /
bindkey -s "^[Ok" "+"
bindkey -s "^[Om" "-"
bindkey -s "^[Oj" "*"
bindkey -s "^[Oo" "/"

bindkey '^R' history-incremental-search-backward

source $XDG_CONFIG_HOME/zsh/my_funs.sh

# setup key accordingly
[[ -n "${key[Home]}"     ]]  && bindkey  "${key[Home]}"     beginning-of-line
[[ -n "${key[End]}"      ]]  && bindkey  "${key[End]}"      end-of-line
[[ -n "${key[Insert]}"   ]]  && bindkey  "${key[Insert]}"   overwrite-mode
[[ -n "${key[Delete]}"   ]]  && bindkey  "${key[Delete]}"   delete-char
[[ -n "${key[Up]}"       ]]  && bindkey  "${key[Up]}"       up-line-or-history
[[ -n "${key[Down]}"     ]]  && bindkey  "${key[Down]}"     down-line-or-history
[[ -n "${key[Left]}"     ]]  && bindkey  "${key[Left]}"     backward-char
[[ -n "${key[Right]}"    ]]  && bindkey  "${key[Right]}"    forward-char
[[ -n "${key[PageUp]}"   ]]  && bindkey  "${key[PageUp]}"   beginning-of-buffer-or-history
[[ -n "${key[PageDown]}" ]]  && bindkey  "${key[PageDown]}" end-of-buffer-or-history

# Fix zsh function keys
bindkey -s "^[OP" ""
bindkey -s "^[OQ" ""
bindkey -s "^[OR" ""
bindkey -s "^[OS" ""
bindkey '^[[3~' delete-char

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
    function zle-line-init () {
        printf '%s' "${terminfo[smkx]}"
    }
function zle-line-finish () {
    printf '%s' "${terminfo[rmkx]}"
}
zle -N zle-line-init
zle -N zle-line-finish
fi


#======================================
#------------Plugins-------------------
#======================================
export ZSH=/usr/share/oh-my-zsh
plugins=(z cargo)
# ZSH autosuggestions
bindkey '^l' autosuggest-accept
bindkey '^n' forward-word
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
. /usr/share/z/z.sh


# vim: set ts=4 sw=4 et:

