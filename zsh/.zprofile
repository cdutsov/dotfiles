export EDITOR=nvim
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export ANDROID_SDK_HOME="$XDG_CONFIG_HOME"/android
export HISTFILE="$XDG_CONFIG_HOME"/zsh/history
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export IPYTHONDIR="$XDG_CONFIG_HOME"/jupyter
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export PYLINTHOME="$XDG_CACHE_HOME"/pylint
export MATHEMATICA_USERBASE="$XDG_CONFIG_HOME"/mathematica
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export XSERVERRC="$XDG_CONFIG_HOME"/X11/xserverrc
export WINEPREFIX="$XDG_DATA_HOME"/wineprefixes/default
export GNUPLOT="$XDG_CONFIG_HOME"/gnuplot
export GNUTERM=wxt
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export JAVA_HOME=/usr/lib/jvm/default/
export PYTHONSTARTUP="$XDG_DATA_HOME"/python_startup.py
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export GEM_HOME="$XDG_DATA_HOME"/gem
export GEM_SPEC_CACHE="$XDG_CACHE_HOME"/gem
export MPD_HOST="$XDG_CONFIG_HOME"/mpd/socket
export NNN_TMPFILE=${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd
export ELECTRUMDIR="$XDG_DATA_HOME/electrum"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass
export SSB_HOME="$XDG_DATA_HOME"/zoom
export ESCDELAY=0
export SYSTEMD_EDITOR=nvim

# if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
#     [[ $(fgconsole 2>/dev/null) == 1 ]] && exec startx "$XDG_CONFIG_HOME"/X11/xinitrc -- vt1 &> /dev/null
# fi
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
    [[ $(fgconsole 2>/dev/null) == 1 ]] && exec startx "$XDG_CONFIG_HOME"/X11/xinitrc -- vt1 &> /dev/null
fi
