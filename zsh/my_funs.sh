# FUZZY
cf() {
    local dir
    dir=$(fd -t d | fzf +m) &&
        cd "$dir"
    }

fcd() {

    file="$(fd | fzf +m)"

    if [[ -n $file ]]
    then
        if [[ -d $file ]]
        then
            cd -- $file
        else
            cd -- ${file:h}
        fi
    fi
}

fr() {
    local file

  #file="$(locate -Ai -0 $@ | grep -z -vE '~$' | fzf --read0 -0 -1)"
  file="$(fd -t f| fzf +m)"

  if [[ -n $file ]]
  then
      if [[ -d $file ]]
      then
          cd -- $file
      else
          cd -- ${file:h}
      fi
  fi
}

# fe [FUZZY PATTERN] - Open the selected file with the default editor
#   - Bypass fuzzy finder if there's only one match (--select-1)
#   - Exit if there's no match (--exit-0)
fe() {
    IFS='
    '
    local declare files=($(fzf-tmux --query="$1" --select-1 --exit-0))
    [[ -n "$files" ]] && ${EDITOR:-vim} "${files[@]}"
    unset IFS
}

# Modified version where you can press
#   - CTRL-O to open with `open` command,
#   - CTRL-E or Enter key to open with the $EDITOR
off() {
    local out file key
    out=$(fzf-tmux --query="$1" --exit-0 --expect=ctrl-o,ctrl-e)
    key=$(head -1 <<< "$out")
    file=$(head -2 <<< "$out" | tail -1)
    if [ -n "$file" ]; then
        [ "$key" = ctrl-e ] && ${EDITOR:-vim} "$file"  || openf "$file"
    fi
}

of() {
    local cmd="${FZF_ALT_C_COMMAND:-"command find -L . \\( -path '*/\\.*' -o -fstype 'dev' -o -fstype 'proc' \\) -prune \
        -o -type d -print 2> /dev/null | sed 1d | cut -b3-"}"
            setopt localoptions pipefail

            out=(${(f)"$(fzf -0 -1 -m)"})
            key=$(head -1 <<< "$out")
            file=$(head -2 <<< "$out" | tail -1)
            if [ -n "$file" ]; then
                [ "$key" = ctrl-e ] && ${EDITOR:-vim} "$file"  || openf "$file"
            fi
        }


    vf() {
        local files

        files=(${(f)"$(locate -Ai -0 $@ | grep -z -vE '~$' | fzf --read0 -0 -1 -m)"})

        if [[ -n $files ]]
        then
            vim -- $files
            print -l $files[1]
        fi
    }

vff() {
    local files

    files=(${(f)"$(fzf -0 -1 -m)"})

    if [[ -n $files ]]
    then
        vim -- $files
        print -l $files[1]
    fi
}

# fkill - kill processes - list only the ones you can kill. Modified the earlier script.
fkill() {
    local pid 
    if [ "$UID" != "0" ]; then
        pid=$(ps -f -u $UID | sed 1d | fzf -m | awk '{print $2}')
    else
        pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')
    fi  

    if [ "x$pid" != "x" ]
    then
        echo $pid | xargs kill -${1:-9}
    fi  
}

if [[ $- == *i* ]]; then

# CTRL-F - Paste the selected file path(s) into the command line
__fsel() {
    local cmd="${FZF_CTRL_F_COMMAND:-"command fd"}"
    setopt localoptions pipefail 2> /dev/null
    eval "$cmd" | FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40%} --reverse $FZF_DEFAULT_OPTS $FZF_CTRL_F_OPTS" $(__fzfcmd) -m "$@" | while read item; do
    echo -n "${(q)item} "
done
local ret=$?
echo
return $ret
}

__fzf_use_tmux__() {
    [ -n "$TMUX_PANE" ] && [ "${FZF_TMUX:-0}" != 0 ] && [ ${LINES:-40} -gt 15 ]
}

__fzfcmd() {
    __fzf_use_tmux__ &&
        echo "fzf-tmux -d${FZF_TMUX_HEIGHT:-40%}" || echo "fzf"
    }

fzf-file-widget() {
LBUFFER="${LBUFFER}$(__fsel)"
local ret=$?
zle redisplay
return $ret
}
zle     -N   fzf-file-widget
bindkey '^F' fzf-file-widget

# ALT-C - cd into the selected directory
fzf-cd-widget() {
local cmd="${FZF_ALT_C_COMMAND:-"command fd -t d"}"
setopt localoptions pipefail
cd "${$(eval "$cmd | $(__fzfcmd) +m $FZF_ALT_C_OPTS"):-.}"
local ret=$?
zle reset-prompt
return $ret
}
zle     -N    fzf-cd-widget
bindkey '\ec' fzf-cd-widget

# CTRL-R - Paste the selected command from history into the command line
fzf-history-widget() {
local selected num
setopt localoptions noglobsubst pipefail
selected=( $(fc -l 1 | eval "$(__fzfcmd) +s --tac +m -n2..,.. --tiebreak=index --toggle-sort=ctrl-r $FZF_CTRL_R_OPTS -q ${(q)LBUFFER}") )
local ret=$?
if [ -n "$selected" ]; then
    num=$selected[1]
    if [ -n "$num" ]; then
        zle vi-fetch-history -n $num
    fi
fi
zle redisplay
return $ret
}
zle     -N   fzf-history-widget
bindkey '^R' fzf-history-widget

fi


# Easy extract
extract () {
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xvjf $1    ;;
            *.tar.gz)    tar xvzf $1    ;;
            *.bz2)       bunzip2 $1     ;;
            *.rar)       rar x $1       ;;
            *.gz)        gunzip $1      ;;
            *.tar)       tar xvf $1     ;;
            *.tbz2)      tar xvjf $1    ;;
            *.tgz)       tar xvzf $1    ;;
            *.zip)       unzip $1       ;;
            *.Z)         uncompress $1  ;;
            *.7z)        7z x $1        ;;
            *)           echo "don't know how to extract '$1'..." ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}


pd () {
    open=xdg-open   # this will open pdf file withthe default PDF viewer on KDE, xfce, LXDE and perhaps on other desktops.

    fd ".pdf$" \
        | fast-p 2>/dev/null \
        | fzf --read0 --reverse -e -d $'\t'  \
        --preview-window down:80% --preview '
            v=$(echo {q} | tr " " "|");
            echo -e {1}"\n"{2} | grep -E "^|$v" -i --color=always;
            ' \
                | cut -z -f 1 -d $'\t' | tr -d '\n' | xargs -r --null $open > /dev/null 2> /dev/null
            }


#zsh calculator
alias eq='noglob eq'; function eq (){echo "$@"| bc -l  | sed '/\./ s/\.\{0,1\}0\{1,\}$//'}
alias q='noglob qalc -c'


# cd into dirs open in terminals
_chd() {
    for PROCID in $(pgrep '^zsh$'); do
        readlink -e /proc/$PROCID/cwd
    done \
        | sort -u \
        | rofi -dmenu
    }
alias chd='cd $(_chd)'

#inkscape function
function svg2tex() {
    inkscape -D -z --file=$1 --export-pdf=`basename $1 .svg`".pdf" --export-latex
}

function openf() {
    xdg-open $1 &> /dev/null
}

function o() {
    xdg-open $1 &> /dev/null
}

function manopt() {
    mn=`man -P cat $1 2> /dev/null || $1 --help`
    shift
    for i in $@
    do
        echo "$mn" |  grep --color=always -A10 "^ *$i" | sed -En '2,${/^[ \t]*-[^-].*$/q};p'
    done
}

function mangrep() {
    mn=`man -P cat $1 || $1 --help` 
    echo $mn | grep --color=always -A5 "$2 "
}

n ()
{
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    # The default behaviour is to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, remove the "export" as in:
    #     NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    # NOTE: NNN_TMPFILE is fixed, should not be modified
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}
