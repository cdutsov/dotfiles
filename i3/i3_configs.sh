#!/bin/sh
setxkbmap -option 'grp:shifts_toggle' -option 'ctrl:nocaps' -layout us,bg -option 'compose:paus'
xmodmap ~/.config/X11/xmodmap &
pgrep xcape || xcape -e 'Control_L=Escape;Shift_R=Up;Alt_R=Left;Control_R=Right'
ps aux | grep '[a]lternating' || /$HOME/.config/i3/alternating_layouts.py &
pkill kbdd
sleep 1 && kbdd
