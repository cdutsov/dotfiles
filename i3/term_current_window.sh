#!/bin/bash

CWD=''

# Get window ID
ID=$(xdpyinfo | grep focus | cut -f4 -d " ")

# Get PID of process whose window this is
PID=$(xprop -id $ID | grep -m 1 PID | cut -d " " -f 3)

# Get last child process (shell, vim, etc)
if [ -n "$PID" ]; then
	TREE=$(pstree -lpA $PID | head -1)
	PROCESS=$(echo $TREE | rev |  cut -d '-' -f -1 | rev)
	PID=$(echo $PROCESS| sed -re 's/[^0-9]//g')
	# If we find the working directory, run the command in that directory
	if [ -e "/proc/$PID/cwd" ]; then
		CWD=$(readlink /proc/$PID/cwd)
	fi
fi
alacritty --working-directory $CWD &

